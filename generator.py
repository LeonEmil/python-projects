def generatePairs(limit):
        num=1
        while num<limit:
                yield num*2
                num = num + 1

returnPairs = generatePairs(10)

print(next(returnPairs))
print(next(returnPairs))

# Ejemplo con yield from

def returnCities(*cities):
    for elements in cities:
        yield from elements

cities = returnCities("Munich", "Paris", "Madrid")

print(next(cities))
print(next(cities))
print(next(cities))()