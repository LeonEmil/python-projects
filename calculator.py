from tkinter import *

padx=15
pady=15
backgroundButton="gray"
fontColor="white"
font=("Helvetica", 16)

root=Tk()
root.title("Calculadora")

frame=Frame()
frame.pack()

screen=Entry(frame)
screen.grid(row=0, column=0, columnspan=4)
screen.config(bg=backgroundButton, fg=fontColor, justify="right", width=20)

#---------------------------------------------First line------------------------------------------------

button1=Button(frame, text="1", command="", padx=padx, pady=pady, bg=backgroundButton, fg=fontColor, font=font)
button1.grid(row=1, column=0)

button2=Button(frame, text="2", command="", padx=padx, pady=pady, bg=backgroundButton, fg=fontColor, font=font)
button2.grid(row=1, column=1)

button3=Button(frame, text="3", command="", padx=padx, pady=pady, bg=backgroundButton, fg=fontColor, font=font)
button3.grid(row=1, column=2)

buttonMult=Button(frame, text="x", command="", padx=padx, pady=pady, bg=backgroundButton, fg=fontColor, font=font)
buttonMult.grid(row=1, column=3)

#---------------------------------------------Second line------------------------------------------------

button4=Button(frame, text="4", command="", padx=padx, pady=pady, bg=backgroundButton, fg=fontColor, font=font)
button4.grid(row=2, column=0)

button5=Button(frame, text="5", command="", padx=padx, pady=pady, bg=backgroundButton, fg=fontColor, font=font)
button5.grid(row=2, column=1)

button6=Button(frame, text="6", command="", padx=padx, pady=pady, bg=backgroundButton, fg=fontColor, font=font)
button6.grid(row=2, column=2)

buttonSub=Button(frame, text="-", command="", padx=padx, pady=pady, bg=backgroundButton, fg=fontColor, font=font)
buttonSub.grid(row=2, column=3)

#---------------------------------------------Third line------------------------------------------------

button7=Button(frame, text="7", command="", padx=padx, pady=pady, bg=backgroundButton, fg=fontColor, font=font)
button7.grid(row=3, column=0)

button8=Button(frame, text="8", command="", padx=padx, pady=pady, bg=backgroundButton, fg=fontColor, font=font)
button8.grid(row=3, column=1)

button9=Button(frame, text="9", command="", padx=padx, pady=pady, bg=backgroundButton, fg=fontColor, font=font)
button9.grid(row=3, column=2)

buttonAdd=Button(frame, text="+", command="", padx=padx, pady=pady, bg=backgroundButton, fg=fontColor, font=font)
buttonAdd.grid(row=3, column=3)

#---------------------------------------------Fourth line------------------------------------------------

buttonPoint=Button(frame, text=".", command="", padx=padx, pady=pady, bg=backgroundButton, fg=fontColor, font=font)
buttonPoint.grid(row=4, column=0)

button0=Button(frame, text="0", command="", padx=padx, pady=pady, bg=backgroundButton, fg=fontColor, font=font)
button0.grid(row=4, column=1)

buttonEqual=Button(frame, text="=", command="", padx=padx, pady=pady, bg=backgroundButton, fg=fontColor, font=font)
buttonEqual.grid(row=4, column=2)

buttonDiv=Button(frame, text="/", command="", padx=padx, pady=pady, bg=backgroundButton, fg=fontColor, font=font)
buttonDiv.grid(row=4, column=3)

root.mainloop()