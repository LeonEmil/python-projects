class Car():
    long=300
    width=250
    wheels=4
    on=False

    def start(self):
        self.on=True

    def state(self):
        if self.on:
            return "El coche está en marcha"
        else:
            return "El coche está apagado"

MyCar=Car()

print(f"El auto tiene {MyCar.wheels} ruedas")

MyCar.start()
print(MyCar.state())

