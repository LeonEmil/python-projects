def adition(num1, num2):
    print(num1 + num2)

def substract(num1, num2):
    print(num1 - num2)

def multiplication(num1, num2):
    print(num1 * num2)

def division(num1, num2):
    try:
        print(num1 / num2)
    except ZeroDivisionError:
        print("No se puede dividir por cero")

num1 = int(input("Digita el primer número: "))
num2 = int(input("Digita el segundo número: "))
operation = input("Elige la operación a realizar: ")

if operation == "suma":
    adition(num1, num2)

if operation == "resta":
    substract(num1, num2)

if operation == "division":
    division(num1, num2)

if operation == "multiplicacion":
    multiplication(num1, num2)